<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

//Poems
Route::get('/','PoemController@index')->name('poems.index');
Route::get('create','PoemController@create')->name('poems.create');
Route::get('about','PoemController@about')->name('poems.about');
Route::get('post','PoemController@post')->name('poems.post');
Route::get('contact','PoemController@contact')->name('poems.contact');
Route::post('poems','PoemController@store')->name('poems.store');
Route::get('poems/{id}/show','PoemController@show')->name('poems.show');
Route::get('/manage','PoemController@manage')->name('poems.manage');
Route::delete('poems/{poem}','PoemController@destroy')->name('poems.destroy');


Route::resource('users', 'UserController');

Route::resource('roles', 'RoleController');

Route::resource('permissions', 'PermissionController');