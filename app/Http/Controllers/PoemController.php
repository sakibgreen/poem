<?php

namespace App\Http\Controllers;

use App\User;
use App\Poem;
use Illuminate\Http\Request;

class PoemController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','clearance'])->except('index','show','about','contact','post');
    }

    public function index()
    {
    	$all_posts = Poem::orderby('id','desc')->paginate(2);
    	return view('poems.index',compact('all_posts'));
    }

    public function create()
    {
    	return view('poems.create');
    }

    public function about()
    {
        return view('poems.about');
    }

    public function post()
    {
        return view('poems.post');
    }

    public function contact()
    {
        return view('poems.contact');
    }

    public function store(Request $request)
    {
    	if ($request->hasFile('image_name')) {
    		$image_name = $request->image_name->getClientOriginalName();
    		$image_size = $request->image_name->getClientSize()/1024/1024;
    		$request->image_name->storeAs('public/upload',$image_name);

    		$poems 						= new Poem;
    		$poems->user_id				= auth()->user()->id;
    		$poems->title 				= $request->title;
    		$poems->image_name			= $image_name;
    		$poems->image_size			= $image_size;
    		$poems->discription			= $request->discription;
    		$poems->save();
    	}
    	return redirect()->route('poems.index');
    }

    public function show($id)
    {
    	$poems		= Poem::find($id);
    	$poems_id	= $poems->id;

    	return view('poems.show',compact('poems','poems_id'));
    }

    public function manage()
    {
    	$poems  = Poem::all();
    	return view('poems.manage',compact('poems'));
    }

    public function destroy($id)
    {
        $poem = Poem::findOrFail($id);
        $poem->delete();

        return redirect()->route('poems.manage')
            ->with('flash_message',
             'Article successfully deleted');
    }

}
