@extends('layouts.app')

@section('title', '| Add Role')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>

    <h1><i class='fa fa-key'></i> Add Role</h1>
    <hr>
    <form class="form-horizontal mt-5" action="{{route('roles.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control">
        </div>

        <h4><b>Assign Permissions</b></h4>

        <div class='form-group'>
            @foreach ($permissions as $permission)
                <input type="checkbox" name="permissions[]" value="{{$permission->id}}">{{$permission->name}} <br>
            @endforeach
        </div>

        <label class="col-md-4 control-label" for=""></label>
            <div class="col-md-4">
                <button class="btn btn-info btn-block"> Add </button>
            </div>
    </form>

</div>

@endsection