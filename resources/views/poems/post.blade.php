@extends('layouts.app')

@section('content')
<!-- Page Header -->
    <header class="masthead" style="background-image: url('img/post-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="post-heading">
              <h1>Man must explore, and this is exploration at its greatest</h1>
              <h2 class="subheading">Problems look mighty small from 150 miles up</h2>
              <span class="meta">Posted by
                <a href="#">Start Bootstrap</a>
                on August 24, 2018</span>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Post Content -->
    <article>
      <!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-preview">
            <h2 class="post-title">
             Simple Post
            </h2>
           
          </div>
            <p>Part of a series on Islam Allah3.svg Beliefs[hide] Oneness of God Prophets Revealed books Angels Predestination Day of Resurrection Practices[hide] Profession of faith Prayer Fasting Alms-giving Pilgrimage Texts and laws[hide] Quran Tafsir Sunnah (Hadith, Sirah) Sharia (law) Fiqh (jurisprudence) Kalam (dialectic) History[hide] Timeline Muhammad Ahl al-Bayt Sahabah Rashidun Imamate Caliphate Spread of Islam Culture and society[hide] Academics Animals Art Calendar Children Demographics Denominations Economics Education Feminism Festivals Finance LGBT Madrasa Moral teachings Mosque Philosophy Poetry Politics Proselytizing Science Social welfare Women Related topics[hide] Criticism of Islam Islam and other religions Islamism Islamophobia Glossary Allah-green.svg Islam portal v t e Islam (/ˈɪslɑːm/)[note 1] is an Abrahamic monotheistic religion teaching that there is only one God (Allah)[1] and that Muhammad is the messenger of God.[2][3] It is the world's second-largest religion[4] and the fastest-growing major religion in the world,[5][6][7] with over 1.8 billion followers or 24.1% of the global population,[8] known as Muslims.[9] Muslims make up a majority of the population in 50 countries.[4] Islam teaches that God is merciful, all-powerful, unique[10] and has guided mankind through prophets, revealed scriptures and natural signs.[3][11] The primary scriptures of Islam are the Quran, viewed by Muslims as the verbatim word of God, and the teachings and normative example (called the sunnah, composed of accounts called hadith) of Muhammad (c. 570–8 June 632 CE). Muslims believe that Islam is the complete and universal version of a primordial faith that was revealed many times before through prophets including Adam, Abraham, Moses and Jesus.[12][13][14] As for the Quran, Muslims consider it to be the unaltered and final revelation of God.[15] Like other Abrahamic religions, Islam also teaches a final judgment with the righteous rewarded paradise and unrighteous punished in hell.[16][17] Religious concepts and practices include the Five Pillars of Islam, which are obligatory acts of worship, and following Islamic law (sharia), which touches on virtually every aspect of life and society, from banking and welfare to women and the environment.[18][19] The cities of Mecca, Medina and Jerusalem are home to the three holiest sites in Islam.[20] Aside from the theological viewpoint,[21][22][23] Islam is historically believed to have originated in the early 7th century CE in Mecca,[24] and by the 8th century the Umayyad Islamic caliphate extended from Iberia in the west to the Indus River in the east. The Islamic Golden Age refers to the period traditionally dated from the 8th century to the 13th century, during the Abbasid Caliphate, when much of the historically Muslim world was experiencing a scientific, economic and cultural flourishing.[25][26][27] The expansion of the Muslim world involved various caliphates and empires, traders and conversion to Islam by missionary activities (dawah).[28] Most Muslims are of one of two denominations:[29][30] Sunni (75–90%)[31] or Shia (10–20%).[32] About 13% of Muslims live in Indonesia,[33] the largest Muslim-majority country, 31% in South Asia,[34][35] the largest population of Muslims in the world,[36] 23% in the Middle East-North Africa,[37] where it is the dominant religion[38] and 15% in Sub-Saharan Africa.[39][40][41] Sizeable Muslim communities are also found in the Americas, the Caucasus, Central Asia, China, Europe, Mainland Southeast Asia, the Philippines, and Russia.[42]</p>
        </div>
      </div>  
    </div>
    <hr>
    </article>

    <hr>

@endsection