@extends('layouts.app')

@section('content')
<!-- Page Header -->
    <header class="masthead" style="background-image: url('img/manage-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="page-heading">
              <h1>Manage your Poems</h1>
              <span class="subheading">Have questions? I have answers.</span>
            </div>
          </div>
        </div>
      </div>
    </header>
<!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-10 mx-auto">
			<table class="table table-dark">
				<thead>
					<tr>
						<th scope="col">Number</th>
						<th scope="col">Title</th>
						<th scope="col">Image</th>
						<th scope="col">Discription</th>
						<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($poems as $poem)
						@if ($poem->user_id == auth()->user()->id)
						<tr>
							<td>{{ $loop->index+1 }}</td>
							<td>{{$poem->title}}</td>
							<td><img src="{{ asset("storage/upload/".$poem->image_name)}}" width="200" height="100"></td>
							<td>{{$poem->discription}}</td>
							<td>
								<a href="#" class="btn btn-warning" style="display: inline;">Edit</a>
								<form action="{{route('poems.destroy',$poem->id)}}" method="post">
									{{csrf_field()}}
									<input type="hidden" name="_method" value="DELETE">
									<button class="btn btn-denger btn-sm">Delete</button>
								</form>
							</td>
						</tr>
						@endif
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection