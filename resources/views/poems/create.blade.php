@extends('layouts.app')

@section('content')
<!-- Page Header -->
    <header class="masthead" style="background-image: url('img/create-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="page-heading">
              <h1>New Poem</h1>
              <span class="subheading">Please keep your Mind</span>
            </div>
          </div>
        </div>
      </div>
    </header>
<div class="container">

	<form class="form-horizontal mt-5" action="{{route('poems.store')}}" method="post" enctype="multipart/form-data">
	{{csrf_field()}}
	<fieldset>
		
		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-12 control-label" for="title">Title</label>  
		  <div class="col-md-12">
		  <input id="title" name="title" type="text" placeholder="Write your title" class="form-control input-md" required="">
		    
		  </div>
		</div>

		<!--Image-->
		<input type="file" name="image_name">

		<!-- Textarea -->
		<div class="form-group">
		  <label class="col-md-12 control-label" for="discription">Discription</label>
		  <div class="col-md-12">                     
		    <textarea class="form-control" id="discription" rows="12" name="discription" required></textarea>
		  </div>
		</div>

		<!-- Button -->
		<div class="form-group">
		  <label class="col-md-4 control-label" for=""></label>
		  <div class="col-md-4">
		    <button class="btn btn-primary btn-sm btn-block">Save</button>
		  </div>
		</div>

	</fieldset>
	</form>
</div>

@endsection

