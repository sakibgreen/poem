@extends('layouts.app')

@section('content')
    <!-- Page Header -->
    <header class="masthead" style="background-image: url('img/home-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              <h1>Clean Blog</h1>
              <span class="subheading">A Blog Theme by Start Bootstrap</span>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          @foreach($all_posts as $poem)
          <div class="post-preview">
            <h2 class="post-title">
              Man must explore, and this is exploration at its greatest
            </h2>
            <img class="card-img-top" src="{{ asset("storage/upload/".$poem->image_name)}}" width="100%" height="350" alt="Card image cap">
          </div>
          <p>{{ str_limit($poem->discription, 300) }} <a href="{{route('poems.show',$poem->id)}}" > Read More.....</a></p>
          @endforeach
        </div>
      </div>  
      <!--paginate-->
      <div class="text-center">
        {!! $all_posts->links() !!}
      </div>
    </div>
    <hr>
@endsection
