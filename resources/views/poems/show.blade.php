@extends('layouts.app')

@section('content')
    <!-- Page Header -->
    <header class="masthead" style="background-image: url('img/show-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="page-heading">
              <h1>Details Poem</h1>
              <span class="subheading">Please keep your Mind</span>
            </div>
          </div>
        </div>
      </div>
    </header>
<!-- Main Content -->
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">
      <div class="post-preview">
        <h2 class="post-title">
          {{ $poems->title }}
        </h2>
        <img class="card-img-top" src="{{ asset("storage/upload/".$poems->image_name)}}" width="100%" height="600" alt="Card image cap">
        <h6>{{$poems->updated_at->diffForHumans()}}</h6>
      </div>
      <p> {{ $poems->discription }} </p>
      <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
   </div>
 </div>  
</div> 

@endsection